const router = require('express').Router();
const User = require('../models/User')
const Joi = require('@hapi/joi');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken')

const schemaRegister = Joi.object({
    name: Joi.string().min(5).max(255).required(),
    email: Joi.string().min(6).max(255).required().email(),
    password: Joi.string().min(6).max(1024).required()
})

// Login
// Guardar Usuarios
const schemaLogin = Joi.object({
    email: Joi.string().min(6).max(255).required().email(),
    password: Joi.string().min(6).max(1024).required()
})
router.post('/login', async (req, res) => {

    // Validaciones de Usuario antes de guardar
    const {error} = schemaLogin.validate(req.body)
    if(error){
        return res.status(400).json({error: error.details[0].message})
    }

    const user = await User.findOne({email: req.body.email})
    if(!user)
        return res.status(400).json({error: "Usuario no encontrado"})
    //Comparar contrasena
    const passValida = await bcrypt.compare(req.body.password, user.password)
    if(!passValida)
        return res.status(400).json({error: "La contrasena es incorrecta"})
     //crear token
     const token = jwt.sign({
         name: user.name,
         id: user._id,
     }, process.env.TOKEN_SECRET)   

     res.header('auth-token', token).json({
        error: null,
        data: {token}
    })
})

// Guardar Usuarios
router.post('/register', async (req, res) => {

    // Validaciones de Usuario antes de guardar
    const {error} = schemaRegister.validate(req.body)
    if(error){
        return res.status(400).json({error: error.details[0].message})
    }

    const existeEmail = await User.findOne({email: req.body.email})
    if(existeEmail){
        return res.status(400).json({error: "email ya se encuentra registrado"})
    }

     // hash contraseña
     const salt = await bcrypt.genSalt(10);
     const password = await bcrypt.hash(req.body.password, salt);

    const user = new User({
        name:req.body.name,
        email:req.body.email,
        password:password,
    })

    try {
        
        const userDb = await user.save();
        res.json({
            error: null,
            data: userDb
        })

    } catch (error) {
        res.status(400).json(error)
    }

    
})

module.exports = router;