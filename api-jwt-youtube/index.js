const express = require('express');
const mongoose = require('mongoose');
const bodyparser = require('body-parser');
require('dotenv').config()

const app = express();

// capturar body
app.use(bodyparser.urlencoded({ extended: false }));
app.use(bodyparser.json());

// Conexión a Base de datos
            
const uri = `mongodb+srv://diegojose:${process.env.PASSWORD}@cluster0.ha0vx.mongodb.net/jwt-directo?retryWrites=true&w=majority`;
mongoose.connect(uri,
    { useNewUrlParser: true, useUnifiedTopology: true }
)
.then(() => console.log('Base de datos conectada'))
.catch(e => console.log('error db:', e))

// import routes
const authRoutes = require('./routes/auth')
const admin = require('./routes/admin');
const verifyToken = require('./midlewares/validatetoken');

// route middlewares
app.use('/api/user', authRoutes)
app.use('/api/admin', verifyToken, admin)


app.get('/', (req, res) => {
    res.json({
        estado: true,
        mensaje: 'funciona!'
    })
});

// iniciar server
const PORT = process.env.PORT || 3001;
app.listen(PORT, () => {
    console.log(`servidor andando en: ${process.env.PORT}`)
})