import Swal from "sweetalert2";
import clienteAxios from "../config/axios";
import {CREAR_USUARIO, CREAR_USUARIO_EXITO, CREAR_USUARIO_ERROR} from "../types";
export function crearUsuarioAction(usuario) {
    return async (dispatch) => {
     
      try {
        // const usuarioCreado = await clienteAxios.post(
        //   "/usuarios",
        //   usuario
        // );
        console.log(usuario)
        dispatch({
          type: CREAR_USUARIO_EXITO
        });
      } catch (error) {
        dispatch({
          type: CREAR_USUARIO_ERROR,
        });
        Swal.fire({
            icon: "error",
            title: "Hubo un error",
            text: error.response.data.errors[0].msg,
          });
      }
    };
  }
  
  const iniciarCrearUsuario = () => ({
    type: CREAR_USUARIO,
  });