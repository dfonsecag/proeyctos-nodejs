import clienteAxios from "../config/axios";
import tokenAuth from "../config/tokenAuth";
import {
    CREAR_TAREA,
    CREAR_TAREA_ERROR,
    CREAR_TAREA_EXITO,
    MOSTRAR_TAREA,
    MOSTRAR_TAREA_ERROR,
    MOSTRAR_TAREA_EXITO,
    SELECCIONAR_TAREA,
    ELIMINAR_TAREA,
    ELIMINAR_TAREA_EXITO,
    ELIMINAR_TAREA_ERROR

} from "../types";

export function crearTareaAction(tarea) {
    return async (dispatch) => {
        dispatch(iniciarCrearTarea());
      const token = localStorage.getItem("token");
      if (token) {
        // Funcion para enviar el token por header
        tokenAuth(token);
      }
      try {
        const tareaCreada = await clienteAxios.post("/tareas", tarea);
        dispatch({
          type: CREAR_TAREA_EXITO,
          payload: tareaCreada.data,
        });
      } catch (error) {
        console.log(error);
        dispatch({
          type: CREAR_TAREA_ERROR,
        });
      }
    };
  }

  const iniciarCrearTarea = () => ({
    type: CREAR_TAREA,
  });

  export function mostrarTareaAction() {
    return async (dispatch) => {
        dispatch(iniciarMostrarTarea());
      const token = localStorage.getItem("token");
      if (token) {
        // Funcion para enviar el token por header
        tokenAuth(token);
      }
      try {
        const tareas = await clienteAxios.get("/tareas");
        //  console.log(tareas.data)
        dispatch({
          type: MOSTRAR_TAREA_EXITO,
          payload: tareas.data,
        });
      } catch (error) {
        console.log(error);
        dispatch({
          type: MOSTRAR_TAREA_ERROR,
        });
      }
    };
  }

  const iniciarMostrarTarea = () => ({
    type: MOSTRAR_TAREA,
  });

  export function tareaSeleccionada(tarea) {
    return async (dispatch) => {
        dispatch({
          type: SELECCIONAR_TAREA,
          payload: tarea
        });
    };
  }

  export function eliminarTareaAction(id) {
    return async (dispatch) => {
        dispatch(iniciarEliminarTarea());
      const token = localStorage.getItem("token");
      if (token) {
        tokenAuth(token);
      }
      try {
          await clienteAxios.delete(`/tareas/${id}`);
        dispatch({
          type: ELIMINAR_TAREA_EXITO,
          payload:id
         
        });
      } catch (error) {
        console.log(error);
        dispatch({
          type: ELIMINAR_TAREA_ERROR,
        });
      }
    };
  }

  const iniciarEliminarTarea = () => ({
    type: ELIMINAR_TAREA,
  });
  
