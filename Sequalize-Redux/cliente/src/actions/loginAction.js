import clienteAxios from "../config/axios";
import tokenAuth from "../config/tokenAuth";
import Swal from "sweetalert2";
import {
  COMENZAR_LOGIN,
  LOGIN_EXITO,
  LOGIN_ERROR,
  ESTA_AUTENTICADO,
  CERRAR_SESION
} from "../types";

// CREAR NUEVO PRODUCTOS
export function loginAction(usuario) {
  return async (dispatch) => {
    dispatch(iniciarLogin());

    try {
      // insertar en la api
      const token = await clienteAxios.post("/login", usuario);
      // si todo sale bien actualizar el state
      dispatch(loginUsuario());
      localStorage.setItem('token',token.data.token );
      
    } catch (error) {
      console.log(error);
      dispatch(loginUsuarioError());
      //alerta de error
      Swal.fire({
        icon: "error",
        title: "Hubo un error",
        text: "Usuario o contrasena invalido",
      });
    }
  };
}
const iniciarLogin = () => ({
  type: COMENZAR_LOGIN,
});
const loginUsuario = (usuario) => ({
  type: LOGIN_EXITO
});
const loginUsuarioError = () => ({
  type: LOGIN_ERROR,
});

export function autenticadoAction() {
  return async (dispatch) => {
    const token = localStorage.getItem("token");
    if (token) {
      // Funcion para enviar el token por header
      tokenAuth(token);
    }
    try {
      const respuesta = await clienteAxios.get("/login/autenticado");
      dispatch({
        type: ESTA_AUTENTICADO,
        payload: respuesta.data.usuario,
      });
    } catch (error) {
      console.log(error);
      dispatch({
        type: LOGIN_ERROR,
      });
    }
  };
}

export function cerrarSesion() {
  return async (dispatch) => {
    localStorage.setItem('token','' );
      dispatch({
        type: CERRAR_SESION
      });
  };
}
