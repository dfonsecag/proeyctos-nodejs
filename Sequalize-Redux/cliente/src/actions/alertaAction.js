import { MOSTRAR_ALERTA, OCULTAR_ALERTA } from "../types";


export function mostrarAlertaAction(alerta) {
    return async (dispatch) => {
        dispatch({
          type: MOSTRAR_ALERTA,
          payload: alerta
        });
    };
  }

  export function ocultarAlertaAction() {
    return async (dispatch) => {
        dispatch({
          type: OCULTAR_ALERTA
        });
    };
  }

