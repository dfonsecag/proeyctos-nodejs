import clienteAxios from "../config/axios";
import tokenAuth from "../config/tokenAuth";
import {
  CREAR_HERRAMIENTA,
  CREAR_HERRAMIENTA_EXITO,
  CREAR_HERRAMIENTA_ERROR,
  MOSTRAR_HERRAMIENTA,
  MOSTRAR_HERRAMIENTA_EXITO,
  MOSTRAR_HERRAMIENTA_ERROR,
  ELIMINAR_HERRAMIENTA,
  ELIMINAR_HERRAMIENTA_EXITO,
  ELIMINAR_HERRAMIENTA_ERROR,
  EDITAR_HERRAMIENTA,
  EDITAR_HERRAMIENTA_ERROR,
  EDITAR_HERRAMIENTA_EXITO,
  SELECCIONAR_HERRAMIENTA,
  DESELECCIONAR_HERRAMIENTA
} from "../types";

export function crearHerramientaAction(herramienta) {
  return async (dispatch) => {
    dispatch(iniciarCrearHerramienta());
    const token = localStorage.getItem("token");
    if (token) {
      // Funcion para enviar el token por header
      tokenAuth(token);
    }
    try {
      const herramientaCreada = await clienteAxios.post(
        "/herramientas",
        herramienta
      );
      dispatch({
        type: CREAR_HERRAMIENTA_EXITO,
        payload: herramientaCreada.data,
      });
    } catch (error) {
      console.log(error);
      dispatch({
        type: CREAR_HERRAMIENTA_ERROR,
      });
    }
  };
}

const iniciarCrearHerramienta = () => ({
  type: CREAR_HERRAMIENTA,
});

export function mostrarHerramienteAction(id) {
  return async (dispatch) => {
    dispatch(iniciarMostrarHerramienta());
    const token = localStorage.getItem("token");
    if (token) {
      // Funcion para enviar el token por header
      tokenAuth(token);
    }
    try {
      const herramientas = await clienteAxios.get(`/herramientas/${id}`);
      dispatch({
        type: MOSTRAR_HERRAMIENTA_EXITO,
        payload: herramientas.data,
      });
    } catch (error) {
      console.log(error);
      dispatch({
        type: MOSTRAR_HERRAMIENTA_ERROR,
      });
    }
  };
}

const iniciarMostrarHerramienta = () => ({
  type: MOSTRAR_HERRAMIENTA,
});

export function eliminarHerramientaAction(id) {
  return async (dispatch) => {
    dispatch(iniciarEliminarHerramienta());
    const token = localStorage.getItem("token");
    if (token) {
      tokenAuth(token);
    }
    try {
      await clienteAxios.delete(`/herramientas/${id}`);
      dispatch({
        type: ELIMINAR_HERRAMIENTA_EXITO,
        payload: id,
      });
    } catch (error) {
      console.log(error);
      dispatch({
        type: ELIMINAR_HERRAMIENTA_ERROR,
      });
    }
  };
}

const iniciarEliminarHerramienta = () => ({
  type: ELIMINAR_HERRAMIENTA,
});

// Metodo para seleccionar una herramienta
export function herramientaSeleccionada(herramienta) {
  return async (dispatch) => {
    dispatch(herramientaSeleccion(herramienta));
  };
}
const herramientaSeleccion = (herramienta) => ({
  type: SELECCIONAR_HERRAMIENTA,
  payload: herramienta,
});

export function herramientadeseleccionar() {
  return async (dispatch) => {
      dispatch({
        type: DESELECCIONAR_HERRAMIENTA
      });
  };
}


export function editarHerramientaAction(herramienta) {
  return async (dispatch) => {
    dispatch(iniciarEditarHerramienta());
    const token = localStorage.getItem("token");
    if (token) {
      // Funcion para enviar el token por header
      tokenAuth(token);
    }
    try {
      const herramientaEditada = await clienteAxios.put(
        `/herramientas/${herramienta.id}`,
        herramienta
      );
      dispatch({
        type: EDITAR_HERRAMIENTA_EXITO,
        payload: herramienta,
      });
    } catch (error) {
      console.log(error);
      dispatch({
        type: EDITAR_HERRAMIENTA_ERROR,
      });
    }
  };
}

const iniciarEditarHerramienta = () => ({
  type: EDITAR_HERRAMIENTA,
});
