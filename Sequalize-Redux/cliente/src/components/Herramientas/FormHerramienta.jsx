import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { crearHerramientaAction, editarHerramientaAction} from "../../actions/herramientaAction";

const FormHerramienta = () => {
  const dispatch = useDispatch();
  const TaskId = useSelector((state) => state.tareas.tareaSeleccionada.id);
  const herramientaSeleccionada = useSelector((state) => state.herramientas.herramientaSeleccionada);
  const crearHerramienta = (herramienta) => dispatch(crearHerramientaAction(herramienta));
  const editarHerramienta = (herramienta) => dispatch(editarHerramientaAction(herramienta));

  // Effect que detecta si hay una herramienta seleccionada para editarla
  useEffect(() => {
    if (herramientaSeleccionada !== null) {
      guardarHerramienta(herramientaSeleccionada);
    } else {
      guardarHerramienta({ title: "" });
    }
  }, [herramientaSeleccionada]);

  // state del formulario
  const [herramienta, guardarHerramienta] = useState({
    title: "",
  });
  // Extraer el nombre del proyecto
  const { title, id } = herramienta;

  // Leer los valores del formulario
  const handleChange = (e) => {
    guardarHerramienta({
      ...herramienta,
      [e.target.name]: e.target.value,
    });
  };

  const onSubmit = (e) => {
    e.preventDefault();
    //validar
    if (title.trim() === "") {
      return;
    }
    if(herramientaSeleccionada === null){
      crearHerramienta({ title, TaskId});
    } else{
      editarHerramienta({title, TaskId, id})
    }
    
    // reiniciar el form
    guardarHerramienta({
      title: "",
    });
  };

  return (
    <div className="formulario">
      <form onSubmit={onSubmit}>
        <div className="contenedor-input">
          <input
            type="text"
            className="input-text"
            placeholder="Nombre Herramienta..."
            name="title"
            value={title}
            onChange={handleChange}
          />
        </div>

        <div className="contenedor-input">
          <input
            type="submit"
            className="btn btn-primario btn-submit btn-block"
            value={herramientaSeleccionada ? "Actualizar Herramienta" : "Agregar Herramienta"}
          />
        </div>
      </form>
      
      {/* {errortarea ? (
        <p className="mensaje error">El nombre de la tarea es obligatorio</p>
      ) : null} */}
    </div>
  );
};

export default FormHerramienta;
