import React, { Fragment, useState } from "react";
import { useDispatch } from "react-redux";
import { crearTareaAction } from "../../actions/tareaAction";

const NuevoProyecto = () => {
  const dispatch = useDispatch();
  const crearTarea = (tarea) => dispatch(crearTareaAction(tarea));

  //   const {formulario, errorformulario, mostrarFormulario, agregarProyecto, mostrarError} = proyectosContext;

  // state para nuevo proyecto guardar datos del formulario
  const [proyecto, guardarProyecto] = useState({
    title: "",
  });
  // Extraer datos
  const { title } = proyecto;

  // funcion onchange para ir almacenando los datos formulario
  const onChangeProyecto = (e) => {
    guardarProyecto({
      ...proyecto,
      [e.target.name]: e.target.value,
    });
  };

  // Funcion onSubmit Proyecto
  const onSubmitProyecto = (e) => {
    e.preventDefault();

    //validar
    if (title === "") {
      // mostrarError();
      return;
    }

    //pasar al action
    crearTarea({ title });

    //reiniciar el formulario
    guardarProyecto({
      title: "",
    });
  };

  return (
    <Fragment>
      <button
        type="button"
        className="btn btn-block btn-primario"
        //   onClick={() => mostrarFormulario()}
      >
        Nueva Tarea
      </button>

      {
       
        <form onSubmit={onSubmitProyecto} className="formulario-nuevo-proyecto">
          <input
            type="text"
            className="input-text"
            placeholder="Nombre tarea"
            name="title"
            value={title}
            onChange={onChangeProyecto}
          />

          <input
            type="submit"
            className="btn btn-primario btn-block"
            value="Agregar Tarea"
          />
        </form>
        
      }

      {
        //   errorformulario ? <p className="mensaje error">El nombre del proyecto es obligatorio</p>  :null
      }
    </Fragment>
  );
};

export default NuevoProyecto;
