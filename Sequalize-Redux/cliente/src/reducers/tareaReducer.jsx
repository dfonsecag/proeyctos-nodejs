import {
  CREAR_TAREA,
  CREAR_TAREA_ERROR,
  CREAR_TAREA_EXITO,
  MOSTRAR_TAREA,
  MOSTRAR_TAREA_ERROR,
  MOSTRAR_TAREA_EXITO,
  SELECCIONAR_TAREA,
  ELIMINAR_TAREA,
  ELIMINAR_TAREA_EXITO,
  ELIMINAR_TAREA_ERROR,
} from "../types";
// cada reducer tiene su propio state
const initialState = {
  error: null,
  loading: false,
  tareas: [],
  tareaSeleccionada: null,
};

export default function (state = initialState, action) {
  switch (action.type) {
    case MOSTRAR_TAREA:
    case CREAR_TAREA:
    case ELIMINAR_TAREA:
      return {
        ...state,
        loading: true,
      };
    case CREAR_TAREA_EXITO:
      return {
        ...state,
        loading: false,
        tareas: [...state.tareas, action.payload],
      };
    case MOSTRAR_TAREA_EXITO:
      return {
        ...state,
        loading: false,
        tareas: action.payload,
      };
    case MOSTRAR_TAREA_ERROR:
    case CREAR_TAREA_ERROR:
    case ELIMINAR_TAREA_ERROR:
      return {
        ...state,
        loading: false,
        error: true,
      };
    case SELECCIONAR_TAREA:
      return {
        ...state,
        tareaSeleccionada: action.payload,
      };
    case ELIMINAR_TAREA_EXITO:
      return {
        ...state,
        tareaSeleccionada: null,
        tareas: state.tareas.filter(
          (tarea) => tarea.id !== action.payload
        ),
      };
    default:
      return state;
  }
}
