import { MOSTRAR_ALERTA, OCULTAR_ALERTA } from "../types";
// cada reducer tiene su propio state
const initialState = {
  alerta: false,
  mensaje: null,
};
export default function (state = initialState, action) {
  switch (action.type) {
    case MOSTRAR_ALERTA:
      return {
        ...state,
        mensaje: action.payload,
        alerta: true,
      };
    case OCULTAR_ALERTA:
      return {
        ...state,
        mensaje: null,
        alerta: false,
      };

    default:
      return state;
  }
}
