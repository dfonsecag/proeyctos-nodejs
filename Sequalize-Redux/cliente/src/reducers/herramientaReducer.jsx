import {
  CREAR_HERRAMIENTA,
  CREAR_HERRAMIENTA_EXITO,
  CREAR_HERRAMIENTA_ERROR,
  MOSTRAR_HERRAMIENTA,
  MOSTRAR_HERRAMIENTA_EXITO,
  MOSTRAR_HERRAMIENTA_ERROR,
  ELIMINAR_HERRAMIENTA,
  ELIMINAR_HERRAMIENTA_EXITO,
  ELIMINAR_HERRAMIENTA_ERROR,
  EDITAR_HERRAMIENTA,
  EDITAR_HERRAMIENTA_ERROR,
  EDITAR_HERRAMIENTA_EXITO,
  SELECCIONAR_HERRAMIENTA,
  DESELECCIONAR_HERRAMIENTA,
} from "../types";
// cada reducer tiene su propio state
const initialState = {
  error: null,
  loading: false,
  herramientas: [],
  herramientaSeleccionada: null,
};
export default function (state = initialState, action) {
  switch (action.type) {
    case MOSTRAR_HERRAMIENTA:
    case CREAR_HERRAMIENTA:
    case ELIMINAR_HERRAMIENTA:
    case EDITAR_HERRAMIENTA:
      return {
        ...state,
        loading: true,
        error: null,
      };
    case CREAR_HERRAMIENTA_EXITO:
      return {
        ...state,
        loading: false,
        error: null,
        herramientas: [...state.herramientas, action.payload],
      };
    case CREAR_HERRAMIENTA_ERROR:
    case MOSTRAR_HERRAMIENTA_ERROR:
    case ELIMINAR_HERRAMIENTA_ERROR:
    case EDITAR_HERRAMIENTA_ERROR:
      return {
        ...state,
        loading: false,
        error: true,
      };

    case MOSTRAR_HERRAMIENTA_EXITO:
      return {
        ...state,
        loading: false,
        error: null,
        herramientas: action.payload,
      };
    case SELECCIONAR_HERRAMIENTA:
      return {
        ...state,
        herramientaSeleccionada: action.payload,
      };
    case DESELECCIONAR_HERRAMIENTA:
      return {
        ...state,
        herramientaSeleccionada: null,
      };
    case EDITAR_HERRAMIENTA_EXITO:
      return {
        ...state,
        herramientaSeleccionada: null,
        herramientas: state.herramientas.map((herramienta) =>
          herramienta.id === action.payload.id
            ? (herramienta = action.payload)
            : herramienta
        ),
      };
    case ELIMINAR_HERRAMIENTA_EXITO:
      return {
        ...state,
        herramientaSeleccionada: null,
        error: null,
        herramientas: state.herramientas.filter(
          (herramienta) => herramienta.id !== action.payload
        ),
      };

    default:
      return state;
  }
}
