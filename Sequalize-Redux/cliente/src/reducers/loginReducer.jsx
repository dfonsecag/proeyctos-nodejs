import {
  COMENZAR_LOGIN,
  LOGIN_EXITO,
  LOGIN_ERROR,
  ESTA_AUTENTICADO,
  CERRAR_SESION,
} from "../types";

// cada reducer tiene su propio state
const initialState = {
  error: null,
  loading: false,
  login: false,
  usuario: null,
};

export default function (state = initialState, action) {
  switch (action.type) {
    case COMENZAR_LOGIN:
      return {
        ...state,
        loading: true,
      };
    case ESTA_AUTENTICADO:
    case LOGIN_EXITO:
      return {
        ...state,
        login: true,
        error: null,
        usuario: action.payload,
      };
    case CERRAR_SESION:
      return {
        ...state,
        login: false,
        error: null,
        usuario: null,
      };
    case LOGIN_ERROR:
      return {
        ...state,
        login: false,
        error: true,
      };
    default:
      return state;
  }
}
