'use strict';

const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Task extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      Task.belongsTo(models.User); 
      Task.hasMany(models.Herramienta); 
    }
  };
  Task.init({
    title: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Task',
  });

   //Obtiene las tareas por un usuario
   Task.tareasPorUsuario = function (UserId) {

    return sequelize.query(

      `select * FROM public."Tasks"`,

      { type: sequelize.QueryTypes.SELECT }
    );

  }

  return Task;
};