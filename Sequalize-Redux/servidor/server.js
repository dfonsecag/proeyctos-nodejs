const express = require("express");

require('dotenv').config({path: 'variables.env'});

const bodyParser = require("body-parser");

const cors = require("cors");

const app = express();

const db = require("./models");
db.sequelize.sync();

// var corsOptions = {
//   origin: "http://localhost:8080"
// };

// app.use(cors(corsOptions));
app.use(cors())
// parse requests of content-type - application/json
app.use(express.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(express.urlencoded({ extended: true }));

// Rutas que se definen del proyecto
require("./routes/user")(app);
require("./routes/task")(app);
require("./routes/herramienta")(app);
require("./routes/auth")(app);

// set port, listen for requests
const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
  console.log(`http://localhost:${PORT}/api/usuarios`);
});
