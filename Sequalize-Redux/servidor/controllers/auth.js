const jwt = require("jsonwebtoken");
const nodemailer = require('nodemailer');
const generator = require('generate-password');
const db = require("../models");
const Usuario = db.User;
//Encriptar contrasena
const bcryptjs = require("bcryptjs");
const { generarJWT } = require("../helper/generarJWT");

exports.login = async (req, res) => {
  const { email, password } = req.body;

  try {
    //Verificar si email existe

    const usuario = await Usuario.findOne({
      attributes: ["id","email", "first_name", "last_name", "password"],
      where: { email },
    });
    if (!usuario) {
      return res.status(400).json({
        msg: "Usuario / Pasword no son correctos -correo",
      });
    }

    const validPassword = bcryptjs.compareSync(password, usuario.password);

    if (!validPassword) {
      return res.status(400).json({
        msg: "Contrasena incorrecta",
      });
    }

    // //Generar el JWT
    const token = await generarJWT(usuario.id);
    res.send({token});
  } catch (error) {
    console.log("error");
    return res.status(500).json(error.message);
  }
};

exports.autenticado = async (req, res) => {
  const token = req.header("x-auth-token");

  //Revisar si no hay token
  if (!token) {
    res.status(401).json(false);
  }

  //Validar el token
  try {
    const cifrado = await jwt.verify(token, process.env.SECRETORPRIVATEKEY);
    const usuario = await Usuario.findOne({
      attributes: ["email", "first_name", "last_name"],
      where: { id: cifrado.id },
    });
    return res.status(200).json({usuario});
  } catch (error) {
    res.status(401).json('Token no valido');
  }
};

exports.recuperarPassword = async (req, res) => {

  const { email } = req.body;


const password = generator.generate({
	length: 10,
	numbers: false
});
  
  // const transporter = nodemailer.createTransport({
  //   service: 'gmail',
  //   auth: {
  //     user: 'diegogarciafonseca@gmail.com',
  //     pass: 'Df563183'
  //   }
  // });
  
  // const mensaje = "Hola desde nodejs...";
  
  // const mailOptions = {
  //   from: 'diegogarciafonseca@gmail.com',
  //   to: email,
  //   subject: 'Recuperacion contrasena SVM',
  //   text: mensaje
  // };
  
  // transporter.sendMail(mailOptions, function(error, info){
  //   if (error) {
  //     console.log(error);
  //   } else {
  //     console.log('Email enviado: ' + info.response);
  //     // res.send(info.response)
  //   }
  // });

  res.send(password)
}