var db = require("../models");
const Tarea = db.Task;
const Herramienta = db.Herramienta;
const errorDb = "Algun error a la hora de conectar a la base de datos";

// Create and Save a new Tutorial
exports.create = async(req, res) => {
  // Validate request
  if (!req.body.title) {
    res.status(400).send({
      message: "No se encontro el titulo de la tarea",
    });
    return;
  }
  // Create a tarea
  const herramienta = {
    title: req.body.title,
    description: req.body.description,
    TaskId: req.body.TaskId,
  };

  try {
    const herramientaIns = await Herramienta.create(herramienta);
    res.send(herramientaIns);
  } catch (err) {
    res.status(500).send({
      message: err.message || errorDb,
    });
  }
};

// Retrieve all Tutorials from the database.
exports.findAll = (req, res) => {
  const title = req.query.title;
  var condition = title ? { title: { [Op.iLike]: `%${title}%` } } : null;

  Herramienta.findAll({
    attributes: ["id", "title","TaskId"],
    // where: {
    //     id: 5
    // },
    // include: [
    //   {
    //     model: Tarea,
    //     attributes: ["id", "title"],
    //   },
    // ],
  })
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || errorDb,
      });
    });
};
// Find a single Tutorial with an id
exports.findOne = async (req, res) => {
  const id = req.params.id;

  try {
    herramienta = await   Herramienta.findAll({
      attributes: ["id", "title","TaskId"],
      where: {
        TaskId: id
      },
      // include: [
      //   {
      //     model: Tarea,
      //     attributes: ["id", "title"],
      //   },
      // ],
    })

    res.send(herramienta);
  } catch (x) {
    res.status(500).send({
          message: x.message || errorDb,
        });
  }
};

// Update a Tutorial by the id in the request
exports.update = async(req, res) => {
  const id = req.params.id;

  try {
    num = await Herramienta.update(req.body, {
      where: { id: id },
    })
    if (num == 1) {
      res.send({
        message: `Herramienta actualizada correctamente`,
      });
    }
      res.send({
        message: `No se encontro la herramienta con el id: ${id}`,
      });
    
  } catch (error) {
    res.status(500).send({
      message: "Error Servidor",
    });
  }
};

// Delete a Tutorial with the specified id in the request
exports.delete = async(req, res) => {
  const id = req.params.id;
  try {
    num = await  Herramienta.destroy({where: { id: id }});
    if (num == 1) {
      res.send({
        message: "Herramienta se elimino exitosamente",
      });
    } else {
      res.send({
        message: `No se encontro la herramienta id: ${id}`,
      });
    }
  } catch (error) {
    res.status(500).send({
      message: "Error en el servidor",
    });
  }   
};
