

  module.exports = app => {
    const { check } = require('express-validator');
    const {login, autenticado, recuperarPassword} = require('../controllers/auth');
    const { validarbody } = require('../midleware/validar-body');



  
    var router = require("express").Router();
  
    router.post('/',[
      check('email', 'El correo es obligatorio').isEmail(),
      check('password', 'La contrasena es requerida').not().isEmpty(),
      validarbody
    ], login);

    router.get('/autenticado', autenticado);
  
    router.post('/recuperar',[
      check('email', 'El correo es obligatorio').isEmail(),
      validarbody
    ], recuperarPassword);
  
    app.use('/api/login', router);
  };