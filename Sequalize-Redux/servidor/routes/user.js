module.exports = app => {
    const usuarios = require("../controllers/user");
    const { check } = require('express-validator');
    const {validarbody} = require('../midleware');
    const {emailExiste} = require('../helper/db-validator');
    const auth = require("../midleware/auth");

  
    var router = require("express").Router();
  
    // Create a new Tutorial
    router.post("/", 
    [
      check('first_name', 'El nombre es obligatorio').not().isEmpty(),
      check('last_name', 'Los apellidos son obligatorio').not().isEmpty(),
      check('email', 'El correo es obligatorio').not().isEmpty(),
      check('email').custom( emailExiste),
      check('email', 'No es un correo valido').isEmail(),
      check('password', 'El password es obligatorio y mas de 6 letras').isLength({min: 6}),
      validarbody
    ]
    ,usuarios.create);
  
    // Retrieve all usuarios
    router.get("/", usuarios.findAll);

    // Retrieve a single Tutorial with id
    router.get("/:id", usuarios.findOne);
  
    // Update a Tutorial with id
    router.put("/:id", usuarios.update);
  
    // Delete a Tutorial with id
    router.delete("/:id",auth ,usuarios.delete);
  
  
  
    app.use('/api/usuarios', router);
  };