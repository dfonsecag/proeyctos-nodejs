const db = require("../models");
const Usuario = db.User;
const Tarea = db.Task;

const emailExiste = async(correo )=> {

    const existeEmail = await Usuario.findOne({attributes: ["id", "email"], where: {email:correo}});
      if(existeEmail){
        console.log(existeEmail)
        throw new Error(`El correo: ${correo} ya esta registrado en la base de datos`)
      }
  }

  module.exports = {
    emailExiste
  }