const { request } = require('express');
const jwt = require('jsonwebtoken');

const generarJWT = (id) => {

    return new Promise ((resolve, request) => {

        const payload = {id};

        jwt.sign(payload, process.env.SECRETORPRIVATEKEY, {
              expiresIn: '2h'
            // expiresIn: 15,
        }, (err, token)=> {
            if(err){
                console.log(err);
                reject('No se puedo generar el token');
            }
            else{
                resolve(token);
            }
        })

    })
}

module.exports = {
    generarJWT
}