
const {Router, response} = require('express');
const { check } = require('express-validator');
const { crearCategoria , obtenerCategorias, obtenerCategoria, actualizarCategoria, borrarCategoria} = require('../controllers/categorias');
const { existeCategoriaPorId } = require('../helpers/db-validators');
const { validarJWT , validarbody, esAdminRole} = require('../middleware');

const router = Router();

// ruta GET   /api/categorias

router.get('/', obtenerCategorias)

//Obtener categoria por id
router.get('/:id', [
  check('id', 'No es un ID Mongo valido').isMongoId(),
  check('id').custom(existeCategoriaPorId),
  validarbody]
, obtenerCategoria);

//Crear categoria privado con un toque valido
router.post('/',[
  validarJWT,
  check('nombre', 'El nombre es obligatorio').not().isEmpty(),
  validarbody
], crearCategoria)

//Actualizar categoria actualizar con un toque valido
router.put('/:id',[
  validarJWT,
  check('nombre', 'El nombre es obligatorio').not().isEmpty(),
  check('id').custom(existeCategoriaPorId),
  validarbody
],actualizarCategoria)

//Eliminar Categoria solo el admministrador con un toque valido
router.delete('/:id',[
  validarJWT,
  esAdminRole,
  check('id', 'No es un ID Mongo valido').isMongoId(),
  check('id').custom(existeCategoriaPorId),
  validarbody

],borrarCategoria)


  
  module.exports = router;