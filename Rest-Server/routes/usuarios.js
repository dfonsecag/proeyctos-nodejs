const {Router} = require('express');
const { check } = require('express-validator');
const { usuariosGet, usuariosPut, usuariosDelete, usuariosPost } = require('../controllers/usuarios');
const { esRolValido, emailExiste, existeUsuarioPorId } = require('../helpers/db-validators');

// const { validarbody } = require('../middleware/validar-body');
// const { validarJWT } = require('../middleware/validar-jwt');
// const { esAdminRole , tieneRole} = require('../middleware/validar-roles');
//   ESTO ES LO MISMO DE ARRIBA CON TAL DE HACER MAS PEQUENO MIDLLEWARES

const {validarbody, validarJWT, esAdminRole, tieneRole} = require('../middleware');



const router = Router();


  router.get('/', usuariosGet);
  
 

  //RUTA ACTUALIZAR USUARIO
  router.put('/:id',[
      check('id', 'No es un ID valido').isMongoId(),
      check('id').custom( existeUsuarioPorId),
      check('rol').custom( esRolValido),
      validarbody
  ], usuariosPut );


  //RUTA POST USUARIO
  router.post('/', [
    // [] midlewares para validar si correo esta bien
    check('nombre', 'El nombre es obligatorio').not().isEmpty(),
    check('password', 'El password es obligatorio y mas de 6 letras').isLength({min: 6}),
    check('correo', 'El correo no es valido').isEmail(),
    check('correo').custom( emailExiste),
    check('rol', 'No es un rol valido').isIn(['ADMIN_ROLE', 'USER_ROLE']),
    check('rol').custom( esRolValido),
    validarbody,
  ], usuariosPost);


  //RUTA DELETE USUARIO
  router.delete('/:id',[    
    validarJWT,
    //esAdminRole,
    tieneRole('ADMIN_ROLE', 'VENTAS_ROLE'),
    check('id', 'No es un ID valido').isMongoId(),
      check('id').custom( existeUsuarioPorId),
      validarbody
  ],  usuariosDelete);

module.exports = router;