
const {Router} = require('express');
const { check } = require('express-validator');
const {login} = require('../controllers/auth');
const { validarbody } = require('../middleware/validar-body');

const router = Router();


  router.post('/login',[
    check('correo', 'El correo es obligatorio').isEmail(),
    check('password', 'La contrasena es requerida').not().isEmpty(),
    validarbody
  ], login);

  module.exports = router;

