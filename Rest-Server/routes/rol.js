const {Router} = require('express');
const { rolesGet } = require('../controllers/usuarios');


const router = Router();

router.get('/', rolesGet);


module.exports = router;