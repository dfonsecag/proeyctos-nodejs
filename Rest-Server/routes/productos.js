
const {Router, response} = require('express');
const { check } = require('express-validator');

const { crearProducto , obtenerProductos, obtenerProducto, actualizarProducto, borrarProducto} = require('../controllers/productos');

const { existeCategoriaPorId, existeUsuarioPorId, existeproductoPorId } = require('../helpers/db-validators');
const { validarJWT , validarbody, esAdminRole} = require('../middleware');

const router = Router();

// ruta GET   /api/productos

router.get('/', obtenerProductos)

//Obtener producto por id
router.get('/:id', [
    check('id', 'No es un ID Mongo valido').isMongoId(),
    check('id').custom(existeproductoPorId),
    validarbody]
  , obtenerProducto);
  
  //Crear producto privado con un toque valido
  router.post('/',[
    validarJWT,
    check('nombre', 'El nombre es obligatorio').not().isEmpty(),
    check('categoria', 'No es un Id de mongo').isMongoId(),
    check('categoria').custom(existeCategoriaPorId),
    validarbody,
    ] , crearProducto);
  
  //Actualizar producto 
  router.put('/:id',[
    validarJWT,
    check('id').custom(existeproductoPorId),
    validarbody
  ],actualizarProducto)
  
  //Eliminar Categoria solo el admministrador con un toque valido
  router.delete('/:id',[
    validarJWT,
    esAdminRole,
    check('id', 'No es un ID Mongo valido').isMongoId(),
    check('id').custom(existeproductoPorId),
    validarbody
  
  ],borrarProducto)
module.exports = router;