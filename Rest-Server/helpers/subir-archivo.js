const { resolve } = require('path');
const path = require('path');

const { v4:uuidv4 } = require('uuid') ;

const subirArchivo = (files, extensionesValidas = ['png', 'jpg', 'jpeg', 'gif'], carpeta = '') => {

  return new Promise ((resolve, reject) => {

    const {archivo } = files;

    const nombreCortado = archivo.name.split('.');  
    //obtener extension archivo jpg txt png entre otros
    const extension = nombreCortado[nombreCortado.length -1];
  
    // Validar extensiones

    if( !extensionesValidas.includes( extension )){
        return reject(`La extension ${extension} no es permitida, ${extensionesValidas}`);
    }
  
      
    // Para subir el archivo
    
    const nombreTemporalArchivo = uuidv4() + '.' + extension;
    const uploadPath = path.join( __dirname , '../uploads/', carpeta , nombreTemporalArchivo );
  
    archivo.mv(uploadPath, (err)=> {
      if (err) {
         reject(err);
      }
      
      resolve(nombreTemporalArchivo);
    });

  });
    
 

}

module.exports = {
    subirArchivo
}