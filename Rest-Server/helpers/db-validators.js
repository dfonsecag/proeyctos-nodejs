

const Role = require('../models/role');

const {Usuario, Categoria, Producto }= require('../models');

const esRolValido = async(rol = '')=> {
    const existerol = await Role.findOne({rol});
    if(!existerol){
      throw new Error(`El rol ${rol} no esta registrado en la base de datos`)
    }
}

const emailExiste = async(correo )=> {

  const existeEmail = await Usuario.findOne({correo});
    if(existeEmail){
      throw new Error(`El correo: ${correo} ya esta registrado en la base de datos`)
    }
}

//Validar que realmente el usuario que voy a obtener en la base de datos exista
const existeUsuarioPorId = async(id)=> {

  const existeUsuario = await Usuario.findById(id);
    if(!existeUsuario){
      throw new Error(`El id : ${id} no existe`)
    }
}
//Validar que realmente la categoria exista en la base de datos
const existeCategoriaPorId = async(id)=> {

  const existeCategoria = await Categoria.findById(id);
    if(!existeCategoria){
      throw new Error(`El id : ${id} no existe`)
    }
}

//Validar que realmente el producto exista en la base de datos
const existeproductoPorId = async(id)=> {

  const existeProducto = await Producto.findById(id);
    if(!existeProducto){
      throw new Error(`El id : ${id} no existe`)
    }
}

// Validar las colecciones permitidas
const coleccionesPermitidas = (coleccion = '', colecciones=[ ]) => {

  const incluida = colecciones.includes(coleccion);
  if(!incluida){
    throw new Error(`La coleccion ${coleccion} no es permitida, ${colecciones}`)
  }

  return true;

}

module.exports = {
    esRolValido,
    emailExiste,
    existeUsuarioPorId,
    existeCategoriaPorId,
    existeproductoPorId,
    coleccionesPermitidas
}