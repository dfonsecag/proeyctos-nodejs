

const validaBody = require('../middleware/validar-body');
const validaJWT = require('../middleware/validar-jwt');
const validaRoles = require('../middleware/validar-roles');
const validarArchivoSubir = require('../middleware/validar-archivo');

module.exports= {
    ...validaBody,
    ...validaJWT,
    ...validaRoles,
    ...validarArchivoSubir
}