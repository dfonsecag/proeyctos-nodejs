const { response } = require("express");
const { ObjectId } = require("mongoose").Types;
const {Usuario, Categoria, Producto} = require('../models')

const colleccionesPermitidas = [
    'usuarios',
    'categorias',
    'productos',
    'roles'
];

const buscarUsuario = async (termino = '', res=response) => {

    const esMongoId = ObjectId.isValid(termino); //si es id mongo manda True

    if( esMongoId){
        const usuario = await Usuario.findById(termino);
        return res.json({
            results: (usuario) ? [usuario] : []
        })
    }

    //Expresion regular
    const regex = new RegExp(termino, 'i');
                                  //.count
    const usuarios = await Usuario.find({
        $or: [{nombre: regex}, {correo: regex}],
        $and: [{estado: true}]
    });
    res.json(usuarios);
}
    //Buscar Categorias
    const categoria = async (termino = '', res=response) => {

        const esMongoId = ObjectId.isValid(termino); //si es id mongo manda True
    
        if( esMongoId){
            const categoria = await Categoria.findById(termino);
            return res.json({
                results: (categoria) ? [categoria] : []
            })
        }
    
        //Expresion regular
        const regex = new RegExp(termino, 'i');
                                      //.count
        const categorias = await Categoria.find({ nombre: regex, estado:true});
        res.json(categorias);
    
    }

  //Buscar Producto
  const buscarProducto = async (termino = '', res=response) => {

    const esMongoId = ObjectId.isValid(termino); //si es id mongo manda True

    if( esMongoId){
        const producto = await Producto.findById(termino).
                                                        populate('categoria', 'nombre');
        return res.json({
            results: (producto) ? [producto] : []
        })
    }

    //Expresion regular
    const regex = new RegExp(termino, 'i');
                                  //.count
    const productos = await Producto.find({ nombre: regex, estado:true}).
                                                        populate('categoria', 'nombre');
    // res.json({
    //     results: productos
    // });
    res.json(productos);

    
}


  const buscar = (req, res=response) => {

    const {coleccion, termino} = req.params;

    if(!colleccionesPermitidas.includes(coleccion)){
        
        return res.status(400).json({
            msg: `las coleciones permitidas son:  ${colleccionesPermitidas}`
        })
    }



    switch (coleccion) {
        case 'usuarios':
            buscarUsuario(termino, res)
            break;
        case 'categorias':
            categoria(termino, res)           
            break;

        case 'productos':
            buscarProducto(termino, res)                 
             break;
        default:
            res.status(500).json({
                msg: 'Se me olvido hacer esta busqueda'
            })
    }

   
}

module.exports = {
    buscar
}