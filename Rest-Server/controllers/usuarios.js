const {response, request} = require('express');
//Encriptar contrasena
const bcryptjs = require('bcryptjs');
// Modelo Usuario
const Usuario = require('../models/usuario');

const Role = require('../models/role')


//OBTENER USUARIOS
  const usuariosGet = async (req = request, res= response) => {
    
    const {limite = 5, desde = 0} = req.query;
    
    const query = {estado:true};
    // const usuarios = await Usuario.find(query)
    //                     .skip(Number(desde))
    //                     .limit(Number(limite));

    // const total = await Usuario.count(query);

    const [total, usuarios] = await Promise.all([

      Usuario.countDocuments(query),
      Usuario.find(query)
                        .skip(Number(desde))
                        .limit(Number(limite))

    ]);

    res.json({total, usuarios})
  }

  //OBTENER Roles
  const rolesGet = async (req = request, res= response) => {
    
    const roles = await Role.find()

    res.json(roles)

  }

  //ACTUALIZAR USUARIO
  const usuariosPut = async (req, res= response) => {

    const {id} = req.params;

    const {_id, password, google, correo, ...resto} = req.body;

    //TODO VALIDAR CONTRA BASE DE DATOS
      if(password){

        //Encryptar contrasena
      const salt = bcryptjs.genSaltSync();
      resto.password = bcryptjs.hashSync(password, salt);
    }

    const usuario = await Usuario.findByIdAndUpdate(id, resto);

    res.json(usuario)
  }

  const usuariosPost = async (req, res = response) => {

    const {nombre, correo, password, rol}  = req.body;
    //nueva instancia de mi usuario
    const usuario = new Usuario({nombre, correo, password, rol});

    //verificar si el correo existe
    // const existeEmail = await Usuario.findOne({correo});
    // if(existeEmail){
    //  return res.status(400).json({
    //    msg:'Este correo ya se encuentra registrado'
    //  })
    // }

    //Encryptar contrasena
    const salt = bcryptjs.genSaltSync();
    usuario.password = bcryptjs.hashSync(password, salt);
    //guardar usuario
    await usuario.save();
    res.json({
        usuario
    })
  }

  //ELIMINAR USUARIO
  const usuariosDelete = async (req, res= response) => {
    const {id}  = req.params;

    

    //Pasa estado false, en vez de eliminar
    const usuario = await Usuario.findByIdAndUpdate(id, {estado: false});
    const usuarioAutenticado = req.usuario;

    res.json("Usuario inactivado");
  }


  module.exports = {
      usuariosGet,
      usuariosPost,
      usuariosDelete,
      usuariosPut,
      rolesGet

  }