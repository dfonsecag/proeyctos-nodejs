
const {response} = require('express');
const Usuario = require('../models/usuario');
//Encriptar contrasena
const bcryptjs = require('bcryptjs');
const { generarJWT } = require('../helpers/generarJWT');


const login = async(req, res = response) => {

    const {correo, password}= req.body;

    try {

        //Verificar si email existe

        const usuario = await Usuario.findOne({correo});
        if(!usuario){
            return res.status(400).json({
                msg: 'Usuario / Pasword no son correctos -correo'
            });
        }

        //si el usuario esta activo estado
        if(!usuario.estado){
            return res.status(400).json({
                msg: 'Se encuentra inactivo'
            });
        }

        //verificar la contrasena

        const validPassword = bcryptjs.compareSync(password, usuario.password);
        if(!validPassword){
            return res.status(400).json({
                msg: 'Contrasena incorrecta'
            });
        }

        //Generar el JWT
        const token = await generarJWT(usuario.id);

        res.json({
            usuario,
            token
        })
        
    } catch (error) {
        console.log('error');
        return res.status(500).json({
            msg: 'Hable con el administrador'
         })
        
    }


}

module.exports = {
    login
}