
const path = require('path');
const fs = require('fs');
const { response } = require("express");    
const { subirArchivo } = require("../helpers");

const {Usuario, Producto} = require('../models');


const cargarArchivo= async(req, res=response)=>{ 

   
  try {
      
    const pathCompleto = await subirArchivo(req.files, undefined, 'textos');

    res.status(200).json({
      nombre: pathCompleto
    });
  } catch (error) {
      res.status(400).json({error});
  }

}

const actualizarImagen = async(req, res=response) => {


    
    const {id, coleccion} = req.params;

    let modelo;

    switch (coleccion) {
        case 'usuarios':
            modelo = await Usuario.findById(id);
            if(!modelo){
                return res.status(400).json({msg: `No existe un usuario con el id: ${id}`})
            }
            break;
            case 'productos':
                modelo = await Producto.findById(id);
                if(!modelo){
                    return res.status(400).json({msg: `No existe un producto con el id: ${id}`})
                }
                break;
    
        default:
            return res.status(500).json({msg: 'Se me olvido validar esto ultimo'});
    }
    
    // Limpiar imagenes previas
    if(modelo.img){
        //hay que borrar  la imagen del servidor
        const pathImagen = path.join(__dirname, '../uploads', coleccion, modelo.img);
        if (fs.existsSync(pathImagen)){
            fs.unlinkSync(pathImagen);
        }
    }

    const nombreImg = await subirArchivo(req.files, undefined, coleccion);
    modelo.img = nombreImg;

    await modelo.save();

    res.json({modelo})
}


// Mostrar imagen

const mostrarImagen = async(req, res= response) =>{

    const {id, coleccion} = req.params;

    let modelo;

    switch (coleccion) {
        case 'usuarios':
            modelo = await Usuario.findById(id);
            if(!modelo){
                return res.status(400).json({msg: `No existe un usuario con el id: ${id}`})
            }
            break;
            case 'productos':
                modelo = await Producto.findById(id);
                if(!modelo){
                    return res.status(400).json({msg: `No existe un producto con el id: ${id}`})
                }
                break;
    
        default:
            return res.status(500).json({msg: 'Se me olvido validar esto ultimo'});
    }
    
    // Limpiar imagenes previas
    if(modelo.img){
        //hay que mostrar la imagen
        const pathImagen = path.join(__dirname, '../uploads', coleccion, modelo.img);
        if (fs.existsSync(pathImagen)){
            return res.sendFile(pathImagen)
        }
    }


    const pathImagen = path.join(__dirname, '../assets/download.png');
    return res.sendFile(pathImagen);
}

module.exports = {
    cargarArchivo,
    actualizarImagen,
    mostrarImagen
}