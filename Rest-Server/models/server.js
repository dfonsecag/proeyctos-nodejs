const express = require('express');
const fileUpload = require('express-fileupload');
const cors = require('cors');
const { dbConnetion } = require('../database/config');
class Server{

    constructor(){
        this.app = express();
        this.port = process.env.PORT;

        //ruta mantemiento usuarios
        //this.usuariosPatch = '/api/usuarios';

        //ruta login
        //this.authPath = '/api/auth';

        this.paths = {
            auth:      '/api/auth',
            buscar:    '/api/buscar',
            categorias:'/api/categorias',
            usuarios: '/api/usuarios',
            productos: '/api/productos',
            uploads: '/api/uploads',
            roles: '/api/roles',
        }

        //CONECTAR A BASE DE DATOS
        this.conectarDB();

        //MIDLEWARES
        this.midlewares();
        //RUTAS DE LA APLACION
        this.routes();
    }

    async conectarDB(){
        
        await dbConnetion();
    }

    midlewares(){

        //CORS
        this.app.use(cors());

        //LECTURA Y PARSEO BODY
        this.app.use(express.json());

        //DIRECTORIO PUBLICO
        this.app.use(express.static('public'));

        //FILEUPLOAD - carga de archivos
        this.app.use(fileUpload({
            useTempFiles : true,
            tempFileDir : '/tmp/',
            createParentPath: true      
        }));

    }

    routes(){
        this.app.use(this.paths.auth, require('../routes/auth'));
        this.app.use(this.paths.usuarios, require('../routes/usuarios'));
        this.app.use(this.paths.categorias, require('../routes/categorias'));
        this.app.use(this.paths.productos, require('../routes/productos'));
        this.app.use(this.paths.roles, require('../routes/rol'));

        this.app.use(this.paths.buscar, require('../routes/buscar'));

        this.app.use(this.paths.uploads, require('../routes/uploads'));
        

           
    }

    listen(){
        this.app.listen(this.port, () => {
            console.log('SERVIDOR CORRIEDO EN PUERTO', this.port)
        });
    }

}

module.exports = Server;