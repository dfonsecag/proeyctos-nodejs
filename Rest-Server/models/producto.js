

const {Schema, model} = require('mongoose');

const ProductoShema = Schema({
    nombre:{
        
        type: String, 
        required: [true, 'El nombre categoria es obligatorio'],
        unique: true
    },
    estado : {
        type:    Boolean,
        default: true,
        required: true
    },
    precio: {
        type: Number,
        default: 0
    
    },
    //Referencia de usuario quien  creo categoria
    usuario : {
        type:    Schema.Types.ObjectId,
        ref: 'Usuario',
        required: true
    },
    //Referencia de Categoria
    categoria : {
        type:    Schema.Types.ObjectId,
        ref: 'Categoria',
        required: true
    },
    description:{
        type: String
    },
    disponible:{
        type:Boolean,
        default: true
    },
    img:{
        type: String
    }

});

ProductoShema.methods.toJSON = function(){
    const {__v, estado, ...data } = this.toObject();
    return data;
}


module.exports = model('Producto', ProductoShema)