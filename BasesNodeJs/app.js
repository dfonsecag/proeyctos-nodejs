const {crearArchivo} = require('./Helpers/multiplicar');
const agrv = require('./config/yargs');
const colors = require('colors');

console.clear();



crearArchivo(agrv.b, agrv.l, agrv.h)
                    .then(nombreArchivo => console.log(nombreArchivo.rainbow, 'creado'))
                    .catch(err => console.log(err))