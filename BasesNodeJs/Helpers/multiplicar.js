
const { rejects } = require('assert');
const fs = require('fs');
const colors = require('colors');


const crearArchivo = async(base = 5, listar = false, hasta = 10) => {
    try {

        let salida ='';
        for (let i = 1; i <= hasta; i++) {
            salida += `${base} * ${i} = ${i*base}\n`;
        }

        if (listar){
            console.log(`==========================
            TABLA DE ${base}
            ========================`);
            console.log(salida);
        }

        fs.writeFileSync(`./salida/tabla-${base}.txt`, salida);
        return `tabla-${base}.txt creada`;
    } catch (error) {
        throw error;
    }
}




module.exports = {
    crearArchivo: crearArchivo
}