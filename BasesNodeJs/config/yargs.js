

const agrv = require('yargs')
                            .option('b', {
                                alias: 'base',
                                type: 'number',
                                demandOption: true,
                                describe: 'Es la base de la tabla de multplicar'
                            })
                            .option('l', {
                                alias: 'listar',
                                type: 'boolean',
                                demandOption: true,
                                default: false,
                                describe: 'Si deseo imprimir por consola'
                            })
                            .option('h', {
                                alias: 'hasta',
                                type: 'number',
                                demandOption: true,
                                describe: 'Es la hasta donde la tabla de multplicar'
                            })
                            .check((argv, options)=>{
                                if(isNaN(argv.b)){
                                    throw 'la base tiene que ser un numero'
                                }
                                return true;
                            })
                            .argv;

    module.exports = agrv;