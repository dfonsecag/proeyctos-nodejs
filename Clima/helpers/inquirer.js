const inquirer = require('inquirer');
require('colors');

const preguntas = [
    {
        type: 'list',
        name: 'option',
        message: 'Que desea hacer?',
        choices:[
            {
                value: '1',
                name: `${'1.'.green} Crear Tarea`
            },
            {
                value: '2',
                name: `${'2.'.green} Listar Tareas`
            },
            {
                value: '3',
                name: `${'3.'.green} Listar Tareas Completadas`
            },
            {
                value: '0',
                name: `${'0.'.green} Salir`
            },
        ]

    }
];

const pausa = async() => {
    const question = [
        {
            type: 'input',
            name: 'enter',
            message: `Presine ${'ENTER'.green} para continuar`
        }

    ]
};

