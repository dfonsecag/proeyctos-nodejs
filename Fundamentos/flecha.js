
// Funcion Normal
// function sumar (a,b){
//     return a + b;
// }


// Funcion de Flecha
const sumar = (a, b) => {
    return a + b;
}

const saludar = () => 'Hola Diego';
console.log(sumar(1,2));
console.log(saludar());