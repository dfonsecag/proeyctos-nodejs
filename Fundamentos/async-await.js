

//ARREGLOS

const empleados = [
    {
        id: 1,
        nombre: 'Diego'
    },
    {
        id: 2,
        nombre: 'Ashley'
    },
    {
        id: 3,
        nombre: 'Ariana'
    }
];


const salarios = [
    {
        id: 1,
        salario: 1000
    },
    {
        id: 2,
        salario: 1500
    }
];

const getEmpleado = (id) => {

    const promesa = new Promise((resolve, reject) => {

        const empleado = empleados.find( e =>  e.id === id);
        // if( empleado ) {
        //     resolve( empleado );
        // } else{
        //     reject(`No existe el empleado con id ${id}`);
        // }
        // ES LO MISMO
        (empleado)
        ? resolve(empleado)
        :reject (`No existe el empleado con id ${id}`)
    });
  
    return promesa;
}

const getSalario = (id) => {

    const promesa = new Promise((resolve, reject) => {

        const salario = salarios.find( e =>  e.id === id);
        (salario)
        ? resolve(salario)
        :reject (`No hay salario para el empleado con id ${id}`)
    });
  
    return promesa;
}

const getInfoUsuario = async(id) => {

    try {
        const empleado = await getEmpleado(id);
    const salario = await getSalario(id);
    return `El empleado ${empleado.nombre} recibe por salario $ ${salario.salario}`;
    } 
    
    catch (error) {
        throw error;
    }
    
}

const id = 10;
getInfoUsuario(id)
    .then(msg => console.log(msg))
    .catch(err => console.log(err));