
//ARREGLOS

const empleados = [
    {
        id: 1,
        nombre: 'Diego'
    },
    {
        id: 2,
        nombre: 'Ashley'
    },
    {
        id: 3,
        nombre: 'Ariana'
    }
];


const salarios = [
    {
        id: 1,
        salario: 1000
    },
    {
        id: 2,
        salario: 1500
    }
];

const getEmpleado = (id) => {

    const promesa = new Promise((resolve, reject) => {

        const empleado = empleados.find( e =>  e.id === id);
        // if( empleado ) {
        //     resolve( empleado );
        // } else{
        //     reject(`No existe el empleado con id ${id}`);
        // }
        // ES LO MISMO
        (empleado)
        ? resolve(empleado)
        :reject (`No existe el empleado con id ${id}`)
    });
  
    return promesa;
}

const getSalario = (id) => {

    const promesa = new Promise((resolve, reject) => {

        const salario = salarios.find( e =>  e.id === id);
        (salario)
        ? resolve(salario)
        :reject (`No hay salario para el empleado con id ${id}`)
    });
  
    return promesa;
}

const id = 10;
//  getEmpleado(id)
//         .then(empleado => console.log(empleado.nombre))
//         .catch(err => console.log(err));

//  console.log('Obteniendo salarios');
//  getSalario(id)
//         .then(salario => console.log(salario.salario))
//         .catch(err => console.log(err));

// getEmpleado(id)
// .then(empleado => {
//     getSalario(id)
//     .then(salario => {
//         console.log('El empleado: ', empleado.nombre, 'tiene un salario de: ', salario.salario);
//     })
//     .catch(err => console.log(err))
// })
// .catch(err => console.log(err))

//  PROMESAS EN CADENA
let nombreEmpleado
getEmpleado(id)
    .then(empleado => {
        nombreEmpleado = empleado.nombre;
        return getSalario(id)
    })
    .then (salario => console.log('El empleado: ', nombreEmpleado, 'tiene un salario de: ', salario.salario))
    .catch(err => console.log(err));