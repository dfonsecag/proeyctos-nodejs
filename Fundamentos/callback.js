

// setTimeout(function(){
//     console.log('Hola Mundo');
// }), 1000;

//Funcion de flecha
setTimeout(()=>{
    console.log('Hola Funcion Flecha');
}), 1000;

const getUsuario = (id, callback) => {
    const user  = {
        id,
        nombre: 'Diego'
    }

    setTimeout( () => {
        callback(user);
    }, 500)
}

getUsuario(20, (usuario) => {
    console.log(usuario.id);
    console.log(usuario.nombre.toUpperCase());
});