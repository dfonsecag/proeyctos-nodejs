
const deappool = {
    nombre: 'Diego',
    apellido: 'Garcia',
    poder: 'Ing',

    getNombre(){
        return `${this.nombre} ${this.apellido} ${this.poder}`;
    }
}

// Crear varias variables
// const nombre = deappool.nombre;
// const apellido = deappool.apellido;
// const poder = deappool.poder;

//Desestrutaracion
// const {nombre, apellido, poder, edad = 25} = deappool;

// console.log(nombre, apellido, poder, edad);

//
// function imprime(deappool){
//     const {nombre, apellido, poder, edad = 25} = deappool;
//     console.log(nombre, apellido, poder, edad);
// }

//
function imprime({nombre, apellido, poder, edad = 25}){
    console.log(nombre, apellido, poder, edad);
}

imprime(deappool);

console.log('Arreglo');

const mascotas = ['Rocky', 'Confite', 'boby'];
// const h1 = mascotas[0];
// const h2 = mascotas[1];
// const h3 = mascotas[2];

const [,,h3] = mascotas;

console.log(h3);
